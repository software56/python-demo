# python-demo

[[_TOC_]]

Proyecto de demostración para correr un programa en Python en un Runner de GitLab.

Este es un proyecto minimalista, pero que tiene resueltas las dificultades habituales para 

1. configurar una máquina en Python
2. correr un programa
3. publicar información a una página web   
   (https://wpregliasco.gitlab.io/python-demo/)

Para entender de qué estamos hablando, hay que consultar [este tutorial sobre GitLab pipelines](https://gitlab.com/wpregliasco/software-tips/-/wikis/GitLab-Pipelines)

Este proyecto tiene propósitos didácticos, así que hay elementos redundantes e innecesarios para un trabajo en serio.   

Es una invitación tácita para copiarlo y modificarlo a gusto de cada quién.

## Archivos y directorios

Esta es la estructura de archivos de este proyecto:

```
├── README.md
├── requirements.txt  
├── compile.py
├── query.py
├── index_template.html
├── .gitignore
├── .gitlab-ci.yml
└── public
    └── .gitkeep
```

*  `README.md` es este archivo

*  `requirements.txt`  tiene un listado de los módulos de python que requieren instalación. Es una lista de texto, con un módulo por línea, pero admite [una sintaxis avanzada](https://gitlab.com/wpregliasco/software-tips/-/wikis/GitLab-Pipelines#configuraci%C3%B3n-de-la-m%C3%A1quina) que no siempre es necesaria.

*  `compile.py`   es el programa que estamos implementando. 

*  `index_template.html`  es el template de la página web de salida. Está escrito con la [sintaxis de Jinja2](https://jinja.palletsprojects.com/en/2.11.x/templates/).

*  `index.html`  es el template renderizado de la página web de salida.

*  `./public`  es un directorio que debe permanecer vacío, pero que tiene que estar. Lo usa el GitLab para publicar las páginas web. El Git no sincroniza directorios que no contienen files, por eso se crea un archivo llamado `.gitkeep` que no tiene nada, pero asegura que el directorio figure en el repositorio.

*  `.gitignore`   contiene las reglas de lo que no debe ser sincronizado, pero que son archivos que produce el Python para backup y precomplilado. Las reglas que puse no son exhaustivas, pero sirven:

   >  \__pycache__   
   >  .ipynb_checkpoints
   >
   >  index.html
   >
   >  *.env
   >
   >  *.log
   >
   >  *.zip

   Las tres últimas están para no intercambiar con el repositorio los passwords ni los logfiles.

​       Para una implementación pedante, [hay listas con ejemplos de este archivo](https://github.com/github/gitignore).

*  `.gitlab-ci.yml`  es un archivo _yaml_  con la configuración de runners. Este lo discutiremos en detalle en seguidita.

---

## `.gitlab-ci.yml`  comentado

```yaml
stages:
  - Test
  - Run
  - Deploy

testJob:
  stage: Test
  image: "python:3"
  when: always   #never to exclude
  script:
    - pwd
    - ls -la
    - python --version
    - pip list                     ## Installed modules
    - python -c 'help("modules")'  ## All modules
    
runScript:
  stage: Run 
  image: "python:3"
  script:   
    - pip install -r requirements.txt
    - python compile.py
  artifacts:
    paths:
      - index.html
    
pages:
  stage: Deploy
  needs:
    - job: runScript
      artifacts: true
  script:
     - cp ./index.html ./public/index.html
  artifacts:
    paths:
      - ./public
  only:
    - master
```

Esto sólo se entenderá en serio leyendo [el tutorial sobre GitLab pipelines](https://gitlab.com/wpregliasco/software-tips/-/wikis/GitLab-Pipelines) y sus referencias originales. Pero vamos a hacer algunos comentarios de esta aplicación específica. 

Es un archivo `yaml` y eso tiene [una sintaxis bien definida](https://rollout.io/blog/yaml-tutorial-everything-you-need-get-started/).

Cada estructura que comienza con `nombre:`  es un _job_ que indica una acción.  
Con excepción de algunas tareas on nombre reservado, cada _job_ genera una máquina virtual, con clone del repositorio y ejecuta algunos comandos.    

Comentamos uno por uno los trabajos.

### `stages:`

Esta es una palabra reservada.   
Si no estuviera, todos los trabajos se ejecutan simultáneamente.   
Esta intrucción hace que los trabajos de un mismo stage se ejecuten simultáneamente, pero que no pasen al stage siguiente hasta que no se completen todos sus procesos.

### `testJob:`

Este proceso está sólo para fines didácticos y de debug. No es realmente necesario, pero lista los recursos que hay disponibles en una máquina virtual. 

* `stage: Test`  
  define el stage del proceso
* `image: "python:3"`  
  define una imagen de Docker.  Si no se especifica la imagen, es algún tipo de máquina que acepta comandos linux. Se pueden elegir una variedad de [imágenes de python](https://hub.docker.com/_/python?tab=description), así como de otras imágenes precompiladas de Docker.

* `when: always|never`  
  establece condiciones para que este proceso corra. Hay sintaxis muy complicadas para estos condicionales. Esto está sólo para anular el proceso cuando estoy seguro de que el resto funcione. 

* `script:`   
  a continuación hay una serie de comandos a ejecutar en la terminal de la máquina virtual.
### `runScript:`

Este es el proceso importante del proyecto. El programa `compile.py` baja información de internet, completa el template y genera un archivo `index.html`  .   
La novedad de este trabajo es la declaración:

*  `artifacts:`    
   Esto es necesario porque el trabajo corre en una máquina virtual con una estructura de directorios clonada.  El problema es que el proceso genera un archivo de salida (index.html) que va a desaparecer junto con la máquina virtual que lo generó. Estos archivos nuevos se llaman _artifacts_ y tengo que declarar explícitamente que los quiero preservar de alguna manera. 
   Aún así, no aparecerán nunca en el repositorio. 

### `pages:`

El job  `pages` es una palabra reservada para designar un proceso que publica la página web del proyecto.

 El detalle de este proceso está en la declaración

```
  needs:
    - job: runScript
      artifacts: true
```

Al declarar que este proceso requiere que que haya corrido previamente el proceso `runscript`  estoy asegurándome el orden de ejecución (que en este caso es casi innecesario porque están en diferentes stages) __pero admás estoy recibiendo los artifacts que generó el proceso del que depende.__  La declaración `artifacts: true` es el default, pero la incluimos para que quede explícito este propósito.

Por último, el proceso copia el archivo que nos interesa al directorio `./public`  y nos aseguramos que los artifacts de esa copia no desaparezcan.  

Será el GitLab el que se encargue de hacer que la página aparezca online con el url:  https://wpregliasco.gitlab.io/python-demo/

---

## Correr el Pipeline

Hay tres maneras de ejecutar el pipeline:

*  se ejecuta automáticamente al hacer un push.
*  se puede ejecutar a mano desde la página CI/CD del proyecto
*  se puede agendar un pipeline para que se ejecute cada cierto tiempo.   
   Se especifica con la [sintaxis del cron](https://github.com/floraison/fugit) y lo configuré para que corra 4 veces por día:  `13 7,12,18,23 * * *`
*  a través de una API

---

## APIs

### Gestión y uso de claves

Las claves serán usadas en la máquina local, y por una cuestión de seguridad, no tienen que ser subidas al servidor. Sólo las usaremos para que los clientes interactúen con el servidor. 

Para operar necesitamos dos tokens:

 *   `PRIVATE-TOKEN`esta clave es equivalente al password de GitLab, pero podemos gestionarla con restricciones de uso. 

     >  1. Log in to GitLab.
     >  2. In the upper-right corner, click your avatar and select **Settings**.
     >  3. On the **User Settings** menu, select **Access Tokens**.
     >  4. Choose a name and optional expiry date for the token.
     >  5. Choose the [desired scopes](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#limiting-scopes-of-a-personal-access-token). (elegir: __read_api__)
     >  6. Click the **Create personal access token** button.
     >  7. Save the personal access token somewhere safe. Once you leave or refreshthe page, you won’t be able to access it again.

* `TRIGGER-TOKEN` es una clave para disparar el pipeline. Exclusiva para este proyecto.

  >  menu: __Settings > CI/CD > Pipeline Triggers__

Guardamos ambos tokens en un archivo local llamado `tokens.env`

```bash
PRIVATE_TOKEN="fRGJsdnvkfDRadFdfops"
TRIGGER_TOKEN="j4klnffbd983DFn4V2aVHIJHD6mldd"
```

Nos aseguramos que esté este archivo excluido por el `.gitignore`

Antes de llamar una API, ejecutamos el comando:

```bash
source tokens.env
```

y así tenemos las variables de environment definidas para el resto de la sesión.

### Correr un pipeline

Se puede hacer un trigger de un pipeline llamando a una API.

El pedido en formato POST es:

```bash
curl -X POST \
     -F token=$TRIGGER_TOKEN  \
     -F ref=master   \  
   https://gitlab.com/api/v4/projects/19557740/trigger/pipeline
```

>  el número que va después de `/projects/` es el id del proyecto que aparece en la página principal.

El comando devuelve un JSON con algunos campos interesantes:

```json
{ 
 "created_at" : "2020-06-29T02:04:50.173Z", #isoformat datetime
 "id" : 160975634,                          #id del pipeline
 "web_url" : "https://gitlab.com/wpregliasco/python-demo/-/pipelines/160974429", #url de los pipelines del proyecto
}
```



### Consultar estado

Para ver si terminó el proceso o hubo un problema, podemos usar la API:

```bash
curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/:proj_id/pipelines/:pipe_id"
```

El comando devuelve un JSON en el que me interesa:

```json
{
    "status" : "success", 
               #[running, pending, success, 
               # failed, canceled, skipped, 
               # created, manual]
   "duration" : 140,  #secs
   "finished_at" : "2020-06-29T02:07:14.461Z",
}
```

### Descargar artifacts

Esto descarga el artifact del último trabajo que funcionó.

```bash
curl --output artifacts.zip  --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" -L "https://gitlab.com/api/v4/projects/:project_id/jobs/artifacts/master/download?job=runScript"
```

### El sercicio de dos centavos

Agregamos el código `query.py` que realiza lo siguiente:

*  dispara un pipeline
*  hace poll hasta que ve que terminó el proceso
*  descarga el artifact

Es una estrategia lenta y se puede hacer toda online, pero es interesante, gratis y funciona.

---

_Willy Pregliasco, 06-2020_



