import os
import datetime as dt
from jinja2 import Template,Environment,meta
import requests
import json
verb = True

def whereami():
    ''' returns basedir
    In local machine my project is in /home/willy/...
    In gitLab server is               /builds/wpregliasco/{projectName}
    '''
    # mapping dictionary: out[startswith]=basedir
    dmap={'/home/willy/Projects/':'/home/willy/Projects/software/python-GitLab',
          '/builds/wpregliasco/' :'/builds/wpregliasco/python-demo',
         }
        
    # select scenario
    cwd = os.getcwd()
    for root in dmap:
        if cwd.startswith(root):
            return dmap[root]
    return None
    
def now():
    ''' returns timestamp
    In isoformat timestamp
    In local time human-readable format
    '''
    t    = dt.datetime.now(dt.timezone.utc)           # datetime object in UTC
    mytz = dt.timezone(dt.timedelta(hours=-3))        # timezone UTC-3
    th   = t.astimezone(mytz)                         # datetime in UTC-3
    t_string = th.strftime('%d/%m/%Y  %I:%M %p (%Z)') # local time string
    
    return t.isoformat(),t_string,th

def loadTemplate(fname):
    '''loads a Jinja template
       Inputs
           fname:    filename of template
       Returns
           template: a Jinja Template object
           var     : a listo of variable needed to fill the template
    '''
    with open(fname,'r') as f:
        infile = f.read()
        
    template = Template(infile)

    '''the list of variables is only needed for debug 
       or advanced implementations
    '''
    env = Environment()
    ast = env.parse(infile)
    var=meta.find_undeclared_variables(ast)

    return template,list(var)

def fillTemplate(**kwargs):
    '''fills the template
    '''
    # filenames
    fin = 'index_template.html'
    fout= 'index.html'
    
    # load template
    T,vars = loadTemplate(fin)
    print('vars:',vars)
    
    # make dictionary of vars
    dic=kwargs
    iso,sdate,dtime = now()
    dic['date'] = dtime
    print('dtimeFill:',dtime)
    
    # fill and write
    T.stream(dic).dump(fout)
    
def loadXkcd():
    '''loads last xkcd data
    '''
    apiXkcd = 'http://xkcd.com/info.0.json' 
    response = requests.get(apiXkcd)
    if response:
        data = response.json()
        if verb:
            print('Xkcd:',data['title'])
        return data
    if verb:
        print('no Xkcd !')
    return {}

def loadNews():
    '''loads last 5 news from Google
    '''
    apiNewsKey = 'f6ad7e2d48704435a3e02039c47e724f'
    apiNews    = f'http://newsapi.org/v2/top-headlines?sources=google-news-ar&apiKey={apiNewsKey}'

    response = requests.get(apiNews)
    if response:
        data = response.json()
        if data['status']=='ok':
            if verb:
                for new in data['articles'][:5]:
                    print('---> ',new['title'])
            return data['articles'][:5]
    if verb:
        print('no News !')        
    return []

def main():
    ## debugs
    basedir = whereami()
    print('my func:',basedir)
    print('cwd    :',os.getcwd())
    iso,sdate,dtime = now()
    print('iso:',iso)
    print('sdate',sdate)

    ## load data and fill template
    fillTemplate(xkcd=loadXkcd(),news=loadNews())
    
    print('done.')
    
if __name__ == '__main__':
    main()
