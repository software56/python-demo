import os
import json
import time
import requests
import datetime as dt

proj_id = '19557740'
# load token as environment variables
from dotenv import load_dotenv   
load_dotenv('tokens.env')                    

private_token = os.environ.get('PRIVATE_TOKEN')
trigger_token = os.environ.get('TRIGGER_TOKEN')

def trigger(verb=True):
    '''triggers the pipeline process
    returns json with pipeline data
    '''
    url = f'https://gitlab.com/api/v4/projects/{proj_id}/trigger/pipeline'
    mydata = {'token': trigger_token, 'ref':'master'}

    out = requests.post(url, data = mydata).json()
    
    if verb:
        Time    = dt.datetime.fromisoformat(out["created_at"][:-1])
        Time    = Time -dt.timedelta(hours=3)
        timeStr = Time.strftime("%H:%M  %d/%m/%Y") 
        print(f'  Triggrered Pipeline ID: {out["id"]}')
        print(f'  Pipeline URL:           {out["web_url"]}')
        print(f'  Started at:             {timeStr}')
    return out

def pollStatus(job_id,verb=True):
    '''polls pipeline status
    returns json with pipeline data
    '''
    # times
    first_wait = 2*60+25 #seconds
    wait = 15            #seconds
    time.sleep(first_wait)
    
    url = f'https://gitlab.com/api/v4/projects/{proj_id}/pipelines/{job_id}'
    myheader = {'PRIVATE-TOKEN': private_token}
    
    while True:
        out = requests.get(url, headers = myheader).json()
        if out['status'] not in ['running', 'pending']:
            break
        time.sleep(wait)
        if verb:
            print ('   Not ready...',flush=True)
    
    if verb:
        Time    = dt.datetime.fromisoformat(out["finished_at"][:-1])
        Time    = Time -dt.timedelta(hours=3)
        timeStr = Time.strftime("%H:%M  %d/%m/%Y") 
        print (f'  Status:                 {out["status"]}')
        print (f'  Finished at:            {timeStr}')
        
    return out
 
def downloadArtifacts(fout='artifacts.zip',verb=True):
    '''download the artifacts of last job
    '''
    url = f'https://gitlab.com/api/v4/projects/{proj_id}/jobs/artifacts/master/download?job=runScript'
    myheader = {'PRIVATE-TOKEN': private_token}
    out = requests.get(url, headers = myheader)
    
    with open(fout, 'wb') as f:
        f.write(out.content)
    return out

#res_trig = trigger()
#job_id = res_trig["id"]
#res_poll = pollStatus(job_id)

downloadArtifacts()
print('done.')

#print(res)

